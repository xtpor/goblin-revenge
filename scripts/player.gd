extends KinematicBody2D

const SCENE_GAMEOVER = "res://scenes/gameover.tscn"
const SCENE_CLEARED = "res://scenes/level_cleared.tscn"

export (PackedScene) var projectile
export (PackedScene) var particle

export (Color) var damage_color
export (int) var max_health
onready var health = max_health

var invicible = false
var coin = 0

var facing = -1
var velocity = Vector2(0, 0)
var jumping = false



func _ready():
	$animation.play("idling")

func _process(delta):
	# Sprite facing direction
	if velocity.x > 0:
		facing = 1
		$sprite.flip_h = true
	elif velocity.x < 0:
		facing = -1
		$sprite.flip_h = false
		
	# Sprite animation
	if jumping:
		set_animation("jumping")
	elif velocity.x == 0:
		set_animation("idling")
	else:
		set_animation("walking")

	if invicible:
		$sprite.modulate = damage_color
	else:
		$sprite.modulate = ColorN("white")
		
	$particles.emitting = velocity.x != 0

func _physics_process(delta):
	# Handle input
	var dir = 0
	if Input.is_action_pressed("move_left"):
		dir -= 1
	if Input.is_action_pressed("move_right"):
		dir += 1
		
	var jump = Input.is_action_pressed("jump")
	var fire = Input.is_action_pressed("fire")
	
	# Handle fire projectile
	if fire and $reload_countdown.is_stopped():
		var p = projectile.instance().shoot_from(self)
		get_node("../../projectiles").add_child(p)

		$reload_countdown.start()
	
	# Force integration
	var force = Vector2(0, 740)
	
	if dir == 0:
		var vsign = sign(velocity.x)
		var vmag = abs(velocity.x)
		
		velocity.x = vsign * max(0, vmag - 1200 * delta)
	elif abs(velocity.x) < 250:
		var magnitude = 900
			
		if sign(velocity.x) != sign(dir):
			magnitude *= 1.65
				
		force.x += dir * magnitude
	
	velocity += force * delta
	velocity = move_and_slide(velocity, Vector2(0, -1))
	
	if jump and is_on_floor():
		$audio_jump.play()
		velocity.y = -480
		jumping = true
	
	if velocity.y == 0:
		jumping = false
		
	if jumping and not jump and velocity.y < -260:
		velocity.y = -260	

func take_damage(amount):
	if health == 0:
		return

	if invicible:
		return false
	else:
		health = max(0, health - amount)
		print("The player takes %s damage." % [amount])
		if health == 0:
			print("player.gd: The player is dead")
			get_node("../../particles").emit(self, particle)
			$audio_gameover.play()
			
			set_process(false)
			set_physics_process(false)
			visible = false
			
			yield(get_tree().create_timer(1.3), "timeout")
			get_tree().change_scene(SCENE_GAMEOVER)
		else:
			$audio_hit.play()
			invicible = true
			$invicibility_countdown.start()
		
		return true

func reach_goal():
	print("Player reaches the goal")
	level_manager.complete_this_level()
	get_tree().change_scene(SCENE_CLEARED)

func pick_up_coin(amount):
	print("Player get %s coin" % [amount])
	$audio_pickup.play()
	coin += amount

func set_animation(clip):
	if $animation.current_animation != clip:
		$animation.play(clip)

func _on_invicibility_countdown():
	invicible = false
