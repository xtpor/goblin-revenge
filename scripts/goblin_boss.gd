extends KinematicBody2D

export (PackedScene) var projectile
export (PackedScene) var particle
export (Color) var damage_color
export (int) var health = 5

var invicible = false
var is_shield_on = true

var velocity = Vector2(0, 0)
var facing = 1

var countdown = 3
var is_moving = false


func _ready():
	start_round()
	print(health)
	

func _process(delta):
	$sprite.flip_h = facing == 1
	
	$shield/particles.emitting = is_shield_on
	$shield/trail.emitting = is_shield_on
	
	if invicible:
		$sprite.modulate = damage_color
	else:
		$sprite.modulate = ColorN("white")

func _physics_process(delta):
	var force = Vector2(0, 740)
	velocity += force * delta
	
	if is_moving:
		velocity.x = facing * 150
	else:
		velocity.x = 0
	velocity = move_and_slide(velocity, Vector2(0, -1))

func start_round():
	$animation.play("walking")
	is_moving = true
	
	for i in range(3):
		while not is_on_wall():
			yield(get_tree(), "physics_frame")
		# print("%s wall" % [i + 1])
		facing = -facing
		yield(get_tree(), "physics_frame")
	
	yield(get_tree().create_timer(0.4), "timeout")
	
	# print("stop moving")
	is_moving = false
	$animation.play("idling")
	
	# Spawn falling stars
	var interval = 0.8 - (6 - health) * 0.1
	var count = (6 - health) * 5

	# print("health=%s, count=%s, interval=%s" % [health, count, interval])
	for i in range(count):
		spawn_star()
		if health <= 2:
			spawn_star()
		yield(get_tree().create_timer(interval), "timeout")
	
	# Re-enable shield
	is_shield_on = false
	yield(get_tree().create_timer(5), "timeout")
	is_shield_on = true
	invicible = false
	
	if health == 0:
		print("Level completed")
		visible = false
		
		get_node("../../particles").emit(self, particle)
		yield(get_tree().create_timer(3), "timeout")
		queue_free()
		
		level_manager.complete_this_level()
		get_tree().change_scene("res://scenes/level_cleared.tscn")
	else:
		start_round()

func spawn_star():
	var r = facing * 64 * rand_range(1, 10)
	var p = projectile.instance()
	p.position = position
	p.position.x = position.x + r
	get_node("../../projectiles").add_child(p)
	# print("spawn star at %s, %s" % [p.position, r])

func take_damage(amount):
	if is_shield_on:
		return false
		
	if not invicible:
		health = max(0, health - amount)
		print("boss health = %s" % [health])
		invicible = true

	
	return true

func _on_invicibility_countdown():
	invicible = false
