extends HBoxContainer

export (float) var scale = 1.0


func _ready():
	set_scale(scale)

func set_scale(ratio):
	print("Set the scale of the ui to %s" % [ratio])
	var buttons = [
		$left/button_left,
		$left/button_right,
		$right/button_jump,
		$right/button_fire
	]
	
	for control in buttons:
		var sprite = control.get_node("button")
		var sprite_size = sprite.normal.get_size()
		
		sprite.scale = Vector2(scale, scale)
		control.rect_min_size = sprite_size * scale