extends Node

const DATA_PATH = "user://audio_settings.json"

var Storage = preload("storage.gd")

var store
var settings

func _ready():
	print("audio settings at %s" % [OS.get_user_data_dir()])
	store = Storage.new(DATA_PATH, {"music": true, "sound": true})
	settings = store.read()
	print("audio settings = %s" % [settings])
	
	set_music(settings.music)
	set_sound(settings.sound)

func set_music(enabled):
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Music"), !enabled)
	settings.music = enabled
	store.write(settings)

func set_sound(enabled):
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Sfx"), !enabled)
	settings.sound = enabled
	store.write(settings)

func is_music_enabled():
	return settings.music

func is_sound_enabled():
	return settings.sound