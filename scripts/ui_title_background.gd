extends Control

func _ready():
	for entity in [$player, $enemy1, $enemy2, $enemy3, $enemy4]:
		entity.get_node("animation").play("walking")
	
	while true:
		for anim in $animation.get_animation_list():
			$animation.play(anim)
			yield($animation, "animation_finished")