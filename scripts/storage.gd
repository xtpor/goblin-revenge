
var _path
var _default_value

func _init(path, default_value):
	_path = path
	_default_value = default_value
	_write_settings_if_not_exist()

func _write_settings_if_not_exist():
	if not File.new().file_exists(_path):
		write(_default_value)

func read():
	var file = File.new()
	if file.open(_path, File.READ) != OK:
		return _default_value
	
	var res = JSON.parse(file.get_as_text())
	if res.error != OK:
		return _default_value
	
	return res.result

func write(data):
	var file = File.new()
	if file.open(_path, File.WRITE) == OK:
		file.store_line(JSON.print(data))
		file.close()