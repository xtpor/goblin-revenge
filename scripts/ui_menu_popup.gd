extends Control

signal about_to_show()
signal popup_hide()

func _ready():
	visible = false

func show():
	emit_signal("about_to_show")
	visible = true
	$panel/margin/rows/toggle_music.pressed = audio_manager.is_music_enabled()
	$panel/margin/rows/toggle_sound.pressed = audio_manager.is_sound_enabled()
	
func hide():
	visible = false
	emit_signal("popup_hide")

func _on_button_back_pressed():
	hide()

func _on_toggle_music_toggled(enabled):
	print("toggle music to %s" % [enabled])
	audio_manager.set_music(enabled)

func _on_toggle_sound_toggled(enabled):
	print("toggle sound to %s" % [enabled])
	audio_manager.set_sound(enabled)

func _on_button_save_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://scenes/splash.tscn")