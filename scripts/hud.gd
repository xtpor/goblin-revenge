extends MarginContainer

export (NodePath) var menu_popup_path
var menu_popup

func _ready():
	menu_popup = get_node(menu_popup_path)
	assert(menu_popup)
	
	menu_popup.connect("about_to_show", self, "_on_menu_about_to_show")
	menu_popup.connect("popup_hide", self, "_on_menu_popup_hide")

func _on_button_pressed():
	menu_popup.show()

func _on_menu_about_to_show():
	$rows/top/button_menu.visible = false
	$rows/joypad.visible = false
	get_tree().paused = true

func _on_menu_popup_hide():
	$rows/top/button_menu.visible = true
	$rows/joypad.visible = true
	get_tree().paused = false
