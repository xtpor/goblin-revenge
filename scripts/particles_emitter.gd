extends Node

func emit(object, particle):
	var p = particle.instance()
	p.init(object)
	add_child(p)
