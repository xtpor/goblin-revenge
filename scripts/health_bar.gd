extends HBoxContainer

export (PackedScene) var icon_heart
export (Color) var color_empty
export (NodePath) var player_path

var player
var icons = []

func _ready():
	assert(icon_heart)
	assert(color_empty)
	assert(player_path)
	
	player = get_node(player_path)
	for i in range(player.max_health):
		var icon = icon_heart.instance()
		add_child(icon)
		icons.append(icon)

func _process(delta):
	for i in range(player.max_health):
		if i < player.health:
			icons[i].self_modulate = ColorN("white", 1.0)
		else:
			icons[i].self_modulate = color_empty
