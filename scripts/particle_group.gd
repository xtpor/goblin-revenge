extends Node2D

const LIFETIME = 4

var timer = 0

func init(entity):
	position = entity.position
	scale.x = -1 * entity.facing

	for child in get_children():
		#if child is Sprite:
		#	child.flip_h = sign(entity.facing) == 1
		
		if child is Particles2D:
			child.emitting = true
	
	return self

func _process(delta):
	timer += delta
	if timer > LIFETIME:
		queue_free()