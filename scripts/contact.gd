extends Area2D

export (int) var damage = 1

func _physics_process(delta):
	for entity in get_overlapping_bodies():
		if entity.is_in_group("character"):
			entity.take_damage(damage)