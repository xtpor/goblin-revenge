extends KinematicBody2D

export (PackedScene) var particle
export (Color) var damage_color
var health = 2

var facing = -1
var velocity = Vector2(0, 0)
var invicible = false

func _ready():
	assert particle != null

func _process(delta):
	$sprite.flip_h = facing == 1
	
	_facing_player()
		
	if invicible:
		$sprite.modulate = damage_color
	else:
		$sprite.modulate = ColorN("white")

func _physics_process(delta):
	var force = Vector2(0, 740)
	velocity += force * delta
	
	velocity = move_and_slide(velocity, Vector2(0, -1))

func take_damage(amount):
	if invicible:
		return false
	else:
		health = max(0, health - amount)
		if health <= 0:
			get_node("../../particles").emit(self, particle)
			queue_free()
		else:
			$audio_hit.play()
			invicible = true
			$invicibility_countdown.start()
		
		return true

func _on_fire_countdown():
	$animation.play("fire")
	
	var p = preload("res://scenes/entities/arrow.tscn").instance()
	p.shoot_from(self)
	get_node("../../projectiles").add_child(p)


func _on_invicibility_countdown():
	invicible = false
	
func _facing_player():
	var entities = get_tree().get_nodes_in_group("player")
	if entities.size() == 0:
		return
	
	var player = entities[0]
	if player.position.x > position.x:
		facing = 1
	else:
		facing = -1
