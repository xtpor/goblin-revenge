extends Control

func _on_button_new_pressed():
	get_tree().change_scene("res://scenes/levels.tscn")

func _on_button_about_pressed():
	get_tree().change_scene("res://scenes/about.tscn")

func _on_button_credits_pressed():
	get_tree().change_scene("res://scenes/credits.tscn")
