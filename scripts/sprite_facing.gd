extends Node

export (NodePath) var sprite_path = NodePath("../sprite")
export (NodePath) var entity_path = NodePath("..")

onready var sprite = get_node(sprite_path)
onready var entity = get_node(entity_path)

func _process(delta):
	if entity.velocity.x > 0:
		sprite.flip_h = true
	elif entity.velocity.x < 0:
		sprite.flip_h = false