extends Sprite

const DECAY_TIME = 1

export (bool) var fade_out = true
export (float) var angular_velocity = 0
export (Vector2) var velocity = Vector2(0, 0)

export (bool) var use_randam_velocity = true

var time = 0

func _ready():
	if use_randam_velocity:
		velocity.y = _random(-150, 50)
		velocity.x = _random(-100, 100)
		angular_velocity = deg2rad(_random(-60, 60))

func _process(delta):
	if fade_out:
		time += delta
		var a = clamp(time, 0, DECAY_TIME) / DECAY_TIME
		self_modulate.a = 1 - ease(a, 0.80)

func _physics_process(delta):
	var force = Vector2(0, 700)
	velocity += force * delta
	position += velocity * delta
	rotation += angular_velocity * delta

func _random(value_min, value_max):
	return randf() * (value_max - value_min) + value_min
