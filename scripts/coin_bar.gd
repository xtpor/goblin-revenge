extends HBoxContainer

export (NodePath) var player_path

var player
var coin_count = 0

func _ready():
	player = get_node(player_path)
	assert(player)

func _process(delta):
	$amount.text = "x" + str(player.coin)