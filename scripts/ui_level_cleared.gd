extends Control

func _on_button_next_pressed():
	level_manager.next_level()

func _on_button_quit_pressed():
	get_tree().change_scene("res://scenes/splash.tscn")
