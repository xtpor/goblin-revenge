extends KinematicBody2D

export (PackedScene) var particle
export (Color) var damage_color
export (int) var health = 2

var invicible = false


func _ready():
	$animation.play("walking")

var velocity = Vector2(0, 0)

var dir = 1
var facing = -1

func _process(delta):
	$sprite.flip_h = dir == 1
	facing = dir
	
	if invicible:
		$sprite.modulate = damage_color
	else:
		$sprite.modulate = ColorN("white")

func _physics_process(delta):
	var force = Vector2(0, 740)
	velocity += force * delta
	
	velocity.x = dir * 150
	velocity = move_and_slide(velocity, Vector2(0, -1))

	if is_on_wall():
		dir = -dir
		
	if is_on_floor() and not $raycast_left.is_colliding():
		dir = 1
		
	if is_on_floor() and not $raycast_right.is_colliding():
		dir = -1

func take_damage(amount):
	if invicible:
		return false
	else:
		health = max(0, health - amount)
		if health <= 0:
			get_node("../../particles").emit(self, particle)
			queue_free()
		else:
			$audio_hit.play()
			invicible = true
			$invicibility_countdown.start()
			
		return true

func _on_invicibility_countdown():
	invicible = false
