extends Control

var buttons

func _ready():
	buttons = $panel/margin/vertical/center/buttons.get_children()
	
	for i in range(buttons.size()):
		var level = i + 1
		
		if level_manager.is_available(level):
			buttons[i].disabled = false
			buttons[i].connect("pressed", level_manager, "select_level", [level])
