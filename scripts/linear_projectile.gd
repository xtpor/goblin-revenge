extends "projectile.gd"

export (float) var speed

var velocity = Vector2(0, 0)

func shoot_from(entity):
	position = entity.position + Vector2(entity.facing * 16, 0)
	facing = entity.facing
	velocity = Vector2(entity.facing * speed, 0)
	return self

func _physics_process(delta):
	position += velocity * delta