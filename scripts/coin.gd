extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	$animation.play("coin")

func _on_collide(entity):
	if entity.is_in_group("player"):
		entity.pick_up_coin(1)
		queue_free()
