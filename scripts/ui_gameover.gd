extends Control

func _on_button_retry_pressed():
	level_manager.retry_level()

func _on_button_quit_pressed():
	get_tree().change_scene("res://scenes/splash.tscn")
