extends Node

const DATA_PATH = "user://level_progress.json"
const scenes = [
	"res://scenes/levels/level1.tscn",
	"res://scenes/levels/level2.tscn",
	"res://scenes/levels/level3.tscn",
	"res://scenes/levels/level4.tscn",
	"res://scenes/levels/level5.tscn",
	"res://scenes/levels/level6.tscn"
]

var Storage = preload("storage.gd")

var store
var _unlocked_level = 1
var _current_level = 1

func _ready():
	store = Storage.new(DATA_PATH, {"progress": 1})
	_unlocked_level = store.read().progress

func is_available(level):
	return 0 < level and \
		level <= scenes.size() and \
		level <= _unlocked_level

func select_level(level):
	assert(is_available(level))
	_current_level = level
	get_tree().change_scene(scenes[level - 1])

func retry_level():
	select_level(_current_level)

func next_level():
	if is_available(_current_level + 1):
		select_level(_current_level + 1)

func complete_this_level():
	if _current_level == _unlocked_level and _current_level != scenes.size():
		_unlocked_level += 1
		store.write({"progress": _unlocked_level})