extends Area2D

var damage = 1
export (PackedScene) var particle

var facing = -1
var velocity = Vector2(400, 0)

func shoot_from(entity):
	position = entity.position
	facing = entity.facing
	velocity.x *= entity.facing
	return self

func _physics_process(delta):
	position += velocity * delta

func _on_collide(body):
	if body.is_in_group("character"):
		body.take_damage(damage)
		queue_free()
	else:
		get_node("../../particles").emit(self, particle)
		queue_free()
