extends Node2D

export (int) var damage = 1

var gravity = Vector2(0, 0)
var velocity = Vector2(0, 0)

var used = false

func _ready():
	start_falling()
	yield(get_tree().create_timer(2), "timeout")
	$trail.emitting = false

func start_falling():
	print("start falling")
	gravity = Vector2(0, 200)
	velocity = Vector2(0, 80)

func _physics_process(delta):
	velocity += gravity * delta
	$body.position += velocity * delta


func _on_collide(body):
	if !used:
		used = true
		$body.visible = false
		
		if body is KinematicBody2D:
			body.take_damage(damage)
		else:
			$explosion.emitting = true
		
		yield(get_tree().create_timer(5), "timeout")
		queue_free()
		
		
