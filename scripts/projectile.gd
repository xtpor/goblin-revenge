extends Node2D

export (PackedScene) var particle
export (int) var damage = 1
var facing = -1

func _on_collide(body):
	if body.is_in_group("character"):
		var hit = body.take_damage(damage)
		if not hit:
			get_node("../../particles").emit(self, particle)
	else:
		get_node("../../particles").emit(self, particle)
	
	queue_free()
