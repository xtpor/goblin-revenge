extends Area2D

func _on_collide(entity):
	if entity.is_in_group("player"):
		entity.reach_goal()
